# The Legend Of Tesla Nandi

Our goal with this project is to make a badass short film, titled _"The Legend of Tesla Nandi"_, and make it entirely in an open source manner.

1. [Folktale Inspiration](#folktale-inspiration)
1. [Why Open Source](#why-open-source)
1. [White TESLA MODEL X plays Nandi](#white-tesla-model-x-plays-nandi)
1. [30 Pages Screenplay](#30-pages-screenplay)
1. [Single-Minded Purpose](#single-minded-purpose)
1. [Team Members](#team-members)
1. [Key Dates](#key-dates)
1. [25,000 Dollars Investment](#25000-dollars-investment)
1. [Release Target](#release-target)
1. [Team Mantra](#team-mantra)

## Folktale Inspiration

Plot inspiration for the short film _"The Legend of Tesla Nandi"_ is an ancient Hindu folktale, a simplified version of which is written here below.

We want our screenplay to be a present day adaptation of all aspects of this ancient tale — its story, its characters, their environment, and so on. Worth repeating that we want our present day adaptation to be _badass_ :smiling_imp: :bomb: :boom: :heart: ... and not bad :confounded: :thumbsdown: :no_entry_sign:

Here is how the folktale goes:

- **Lord Shiva** and his wife **Devi Parvati** had to go to an Art Fair, that was many villages away from their house in the Himalayas.
- They set out on the journey, riding their white Ox, named **Nandi**.

  ![shiv and parvati riding nandi](assets/images/couple-nandi-romantic-300w.png)

- At first, both Shiv and Parvati were seated on Nandi, as they crossed village-1
- People of village-1 started bitching about Shiv and Parvati, _"Look at those inhuman people. Two grown ass adults sitting on one poor animal."_
- Shiv suggested, _"Let me get down from Nandi and walk on foot, so that people don't bitch about us."_ Parvati agreed, because both Shiv and Parvati felt very uncomfortable by other peoples opinions.
- Nandi however said, _"People will always bitch. That's their job. You should only do what you think is right, not what others say."_ But Shiv and Parvati did not listen to Nandi.

---

- So now Shiv was walking on foot, and Parvati was riding on Nandi, as they crossed village-2
- People of village-2 started bitching about Shiv and Parvati, _"Look at that feminazi "queen", enjoying the ride on an Ox, while such a sweet simple husband walks on foot."_
- Parvati suggested, _"Shiv darling, you should sit on Nandi while I walk on foot, so that people don't bitch about us."_ Shiv agreed, because both Shiv and Parvati felt very uncomfortable by other peoples opinions.
- Nandi however said, _"People will always bitch. That's their job. You should only do what you think is right, not what others say."_ But Shiv and Parvati did not listen to Nandi.

---

- So now Parvati was walking on foot, and Shiv was riding on Nandi, as they crossed village-3
- People of village-3 started bitching about Shiv and Parvati, _"Look at that toxic priviledged man, enjoying the ride on an Ox, while such a sweet wife walks on foot."_
- Shiv said to Parvati, _"Honey, maybe we should both walk on foot, so that people don't bitch about us."_ Parvati agreed, because Shiv and Parvati both felt very uncomfortable by other peoples opinions.
- Nandi however said, _"People will always bitch. That's their job. You should only do what you think is right, not what others say."_ But Shiv and Parvati did not listen to Nandi.

---

- So now both Shiv and Parvati were walking on foot alongside Nandi, as they crossed village-4
- People of village-4 started bitching about Shiv and Parvati, and laughed at them so bad, _"Look at that stupid couple, people don't have brains these days. They have such a healthy hardy Ox to ride, but they are walking on foot. Lol."_
- Nandi said, _"See, I told you. No matter what you do, people are always going to bitch. So you should only do what YOU think is right."_
- Shiv and Parvati finally understood Nandi's wisdom, apologized, and bowed down to Nandi with respect. Shiv gave Nandi a boon, _"Dear Nandi, your wisdom is awesome! People shall always worship you first, before they enter the temple to come see us."_

**This being an open source project, you can see precisely how we are crafting a present day screenplay inspired by the above ancient folktale.**

**You can also see details of every other aspect of our filmmaking process.** Just follow along all the [issues](https://gitlab.com/mantra-sd-artists/the-legend-of-tesla-nandi/-/issues?scope=all&utf8=%E2%9C%93&state=all), aka [work items](https://gitlab.com/mantra-sd-artists/the-legend-of-tesla-nandi/-/issues?scope=all&utf8=%E2%9C%93&state=all), in this open source project.

## Why Open Source

_"Open Source"_ is a term borrowed form the world of software creation. In the software world, many apps and tools are created in this manner. For example, creation process of the Chrome browser is entirely open source.

When collaborative work is done in an open source manner:

- All team members can see existing state of current work > access it > alter it > and make progress towards final goal.
- Communication happens in the open (via "issues" and "comments") and can be seen by every team member and general public.
- Task management also happens out in the open (also via "issues" and "comments")
- All team members have ownership and responsibility for success.
- Anybody can work on any task, and team members self-organize.
- Usually one person starts an open source project, but overtime it takes its own life, and becomes a self-sustained team owned project.

A video explanation of how this applies to the software world can be seen here: https://www.youtube.com/watch?v=Oq5wifLQR_U 

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/watch?v=Oq5wifLQR_U" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

**By making _"The Legend of Tesla Nandi"_ in an open source manner, we will empower 1000's of indie artists, and help them get paid long-term film work.** Everybody will be able to see how we wrote screenplay, build a team, raised investment, prepared schedules, did location scouting, film shooting and editing, music creation, and so on.

## White TESLA MODEL X plays Nandi

1. As you can see from the folktale inspiration ([above](#folktale-inspiration)), the ancient ride of Shiva and Parvati is a white Ox, named Nandi. 
1. You can also see that our goal is to make a present day _badass_ adaptation of this ancient Hindu folktale.

Combining these two, and given the tech requirements of future films (listed [below](#single-minded-purpose)), Sumeet finds there is no other ride on our planet today that is — more awesome, more sexy, more wise, and more badass than a **white falcon winged TESLA MODEL X** :heart_eyes:

So of course a _white falcon winged TESLA MODEL X_ has to play the white Ox Nandi in our short film. Subject to how the audition goes :wink:

![](assets/images/model-x-painting-no-border.png)

## 30 Pages Screenplay

Latest copy of the 30 page screenplay can be found

- [in this google drive](https://drive.google.com/drive/folders/1KP9VIVYcpqWm8IKQwYWnPAbrJjy1O8MF?usp=sharing)

Writing progress on various missing parts of the screenplay can seen

- [on this digital planning board](https://gitlab.com/mantra-sd-artists/the-legend-of-tesla-nandi/-/boards/1866253?label_name[]=screenplay-writing), and
- [as step by step RE RE RE WRITING](https://gitlab.com/mantra-sd-artists/the-legend-of-tesla-nandi/-/commits/master/screenplay) process :heart:


## Single-Minded Purpose

As a team, our goal is to generate the best ever ~~&nbsp;paid or purposeful&nbsp;~~ paid and* purposeful work for 4000+ independent motion picuture artists, over the next 15 years or less.

![](assets/images/200w-single-minded-focus-framer.png)

The following films and shows are being written (as of July 1st 2020) to work towards the above purpose.

1. High As A Rocket Ship
1. Learning About America
1. Robbery Drama At Indian Convenience Store Cambridge
1. The Legend Of Tesla Nandi
1. Ayyeki and Mr.Dronn
1. Karma Dharma Classroom
1. First Blood of the Revolution Was Spilled in Massachusetts
1. China, India, America
1. Am Sober AF
1. Can you buy love in 14 cents?
1. बूढा बूढ़ी अब क्या अमरीका घूमेंगे
1. कम ख़र्चे वाली शादी
1. काली कलूटी लड़की
1. ముసలివాడు ముసలావిడ ఇప్పుడు ఏమి అమెరికా తిరుగుతారు
1. The Family Connection App
1. Matatu Business African Style

We will make the 2 hour long movie _"High As A Rocket Ship"_ next — by raising an investment of 4 million dollars, and with a revenue target of 10 million dollars. We will also start making in parallel, the 100 episode Netflix show _"Karma Dharma Classroom"_, but we do not know the investment required and the revenue target for it yet. Let us talk about both these project and more after we are done releasing _"The Legend of Tesla Nandi"_ within budget and deadline. Making _"The Legend of Tesla Nandi"_ short film, **within budget and deadline**, will help us raise investment for other films and shows on the list above.

There is an interesting story about how and why this "single minded" section got added. Here it goes --

> Everybody has their own shortcomings and struggles. Some of Sumeet's many shortcomings are his extreme ADHD, lack of self-discipline, and poor follow-through. His juggling between writing 20+ screenplays, 6+ books, trying to invent new board games, starting product companies, etcetera, was resulting in poor output. So he reached out to an entrepreneur to seek advice for his focus problem. This entrepreneur (for now lets call him _"Mr. Secret Mentor"_) shared a solution that works for him. Talking with Mr. Secret Mentor was immensely helpful, in terms of fixing focus and self-discipline problems, and more. Sumeet has since then decided to prove Mr. Secret Mentor both right and wrong at the same time, and hence this section was added. (Sorry about this cryptic backstory, it will soon make sense, as we start to make films, and all artists meet with Mr. Secret Mentor).

## Team Members

A categorized list of all team members can be found here, in [issue number 5](https://gitlab.com/mantra-sd-artists/the-legend-of-tesla-nandi/-/issues/5)

## Key Dates

| Date | Deadline |
| ------ | ------ |
| Sat, 15 Aug | Finish 30 page screenplay |
| Tue, 18 Aug | Complete 10 versions of opening animation |
| Wed, 26 Aug | Get Tesla X, 20 Other Teslas, & Props |
| Fri, 04 Sep | Begin film shoot |
| Ongoing | Reachout to 120+ movie magazines and websites |
| Wed, 30 Sep | Release on YouTube |
| Ongoing | Team up for High As a Rocket Ship |
| Ongoing | Team up for Karma Dharma Classroom |

## 25,000 Dollars Investment

The socio-economic system of our planet is such — that 99.83% of indie artists _have to do_ multiple non-art jobs to support their life. Not doing so results in them being broke as fuck pretty soon, and that is not recommended.

We clearly need to create more paid work opportunities for indie artists that are directly related to their craft. But doing so is neither easy nor feasible. In the world of films an attempt can be made by thinking on the lines of full economic cycle, and asking as a team — _(1) When and where will the eventual audience pay for our movie? (2) Can we make our film in an amount lower than that revenue target? (3) With those two numbers in place, who now can we raise an investment from and pay all artists on the team? (4) Can we earn investor confidence by getting them a return on their investment?_

For our current short film, those questions can be answered as:

- Eventual audience will NOT pay to watch _"The Legend of Tesla Nandi"_
- We will release our short film on youtube and there will be no ticket prices charged to its audience
- Despite direct revenue target of 0 USD, we are committed to paying all indie artists involved in making this short film
- We will do so by raising an investment of $25,000 dollars; of which we have already raised $5,000 and have gotten a commitment of another $5,000.
- Return on this investment will come through our next movie _"High As A Rocket Ship"_, which requires us to make _"The Legend of Tesla Nandi"_ first — because then we will have a full badass film to show to our investors, and we will be able to raise the 4 million USD required for _"High As A Rocket Ship"_ which in turn will generate direct audience renvenue of over 10 million USD.

## Release Target

[link with details](https://gitlab.com/mantra-sd-artists/the-legend-of-tesla-nandi/-/issues/2)

## Team Mantra

1. Be in startup mode.
2. Love every team member. Fight if you have to, but love everyone first.
3. Look for the genius in each team member (see old dude's quote below).
2. Push audio-visual boundaries of the film without pushing its deadlines.
3. Scheduling is a bitch! Therefore, until the day of release:
    1. Communicate with 1 or more team members everyday
    1. Collaborate with 1 or more team members everyday
2. Jump in and work on multiple tasks
1. [and more ...](https://gitlab.com/mantra-sd-artists/the-legend-of-tesla-nandi/-/issues/2)

![](assets/images/if-you-judge-a-fish.jpg)




